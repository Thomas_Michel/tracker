import hashlib, requests, time, subprocess
from datetime import datetime

# Uses SHA512
def trackerHash(toHash):
    return hashlib.sha512(toHash.encode('utf-8')).hexdigest()

def getFromURL(url):
    return requests.get(url).text

def getTime():
    return int(time.time())

TIME_FORMAT = "%d-%m-%Y %H:%M:%S"

def getTimeFromStr(timeStr):
    return time.mktime(datetime.strptime(timeStr, TIME_FORMAT).timetuple())

def getTimeStrFromInt(timeInt):
    return datetime.fromtimestamp(timeInt).strftime(TIME_FORMAT)

def getTrack():
    for TRACK_URL in TRACK_URLS:
        trackURL = TRACK_URL + READ_ONLY_PASSPHRASE
        try:
            track = getFromURL(trackURL).split("\n")
            return track
        except Exception as exception:
            print(exception)

def isEncrypted(data):
    return not (data.startswith('Battery') or data.startswith('Retrieving'))

def execute(cmd):
    return subprocess.check_output(cmd, shell=True).decode('utf-8')

## Constants:

MY_PASSPHRASE = 'MY_PASSPHRASE'

PASSPHRASES = ['PASSPHRASE_A', 'PASSPHRASE_B', 'PASSPHRASE_C']

TRACKER_PASSPHRASE = 'TRACKER_PASSPHRASE'

TRACK_URLS = [
    'https://yourwebsite0.com/tracker/track.php?key=',
    'https://yourwebsite1.com/tracker/track.php?key=',
]

READ_ONLY_PASSPHRASE = 'READ_ONLY_PASSPHRASE'

BEGIN_TIME = getTimeFromStr('23-09-2022 15:00:00')
# For determinism, always provide as an end time a time for a message that is already received.
END_TIME = getTimeFromStr('25-09-2022 15:00:00')
