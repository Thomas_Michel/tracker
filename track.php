<?php

    $KEY_VARIABLE_ID = 'key';
    $DATA_VARIABLE_ID = 'data';
    $KEYS = [
        'TRACKER_PASSPHRASE' => 'Tracker',
        'READ_ONLY_PASSPHRASE' => 'Read-only',
    ];
    $TRACK_FILE_PATH = '../../priv/track.txt';

    function logg($toEcho, $toWrite)
    {
        global $TRACK_FILE_PATH;
        $toWrite = "\n" . time() . ' ' . $_SERVER['REMOTE_ADDR'] . ' ' . $toWrite;
        file_put_contents($TRACK_FILE_PATH, $toWrite, FILE_APPEND);
        echo $toEcho;
    }

    if(isset($_GET[$KEY_VARIABLE_ID]))
    {
        $keyTested = $_GET[$KEY_VARIABLE_ID];
        if(array_key_exists($keyTested, $KEYS))
        {
            $identifier = $KEYS[$keyTested];
            if($identifier === 'Tracker' && isset($_GET[$DATA_VARIABLE_ID]))
            {
                $data = $_GET[$DATA_VARIABLE_ID];
                logg('Received', $identifier . ' ' . $data);
            }
            else
            {
                echo file_get_contents($TRACK_FILE_PATH);
            }
        }
        else
        {
            echo 'Incorrect key';
        }
    }
    else
    {
        echo 'Key or data not provided';
    }

?>
