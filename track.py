#!/usr/bin/python3

import requests, utils

# Uses AES 256 CBC
def encrypt(toEncrypt, passphrase):
    cmd = f"echo '{toEncrypt}' | openssl aes-256-cbc -pbkdf2 -a -pass pass:{passphrase}"
    return utils.execute(cmd).replace("\n", "")

def getLocation():
    return utils.execute('termux-location')[:-1]

def log(data):
    with open('track.txt', 'a') as f:
        f.write(data + "\n")

def send(data):
    data = requests.utils.quote(data)
    results = []
    for TRACK_URL in utils.TRACK_URLS:
        url = TRACK_URL + utils.TRACKER_PASSPHRASE + '&data=' + data
        try:
            results += [utils.getFromURL(url)]
        except Exception as exception:
            print(exception)
    return results

currentTime = str(utils.getTime())
print(currentTime)
hashes = [utils.trackerHash(passphrase + currentTime) for passphrase in utils.PASSPHRASES]
thresholdHash = ''.join(hashes)
print(thresholdHash)

location = getLocation()
if location != '':
    print(location)
    encryptedLocation = encrypt(location, thresholdHash)
    print(encryptedLocation)

    data = currentTime + ' ' + encryptedLocation
    log(currentTime + ' ' + location)
    send(data)
else:
    RETRIEVING_LOCATION_FAILED_ERROR = 'Retrieving location failed!'
    print(RETRIEVING_LOCATION_FAILED_ERROR)
    log(currentTime + ' ' + RETRIEVING_LOCATION_FAILED_ERROR)
    send(currentTime + ' ' + RETRIEVING_LOCATION_FAILED_ERROR)

