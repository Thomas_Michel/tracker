#!/usr/bin/python3

import utils

track = utils.getTrack()
linesLen = len(track)

TIME_FIELDS = [0, 3]

with open('readible.txt', 'w') as f:
    for linesIndex in range(linesLen):
        line = track[linesIndex]
        lineParts = line.split()
        for TIME_FIELD in TIME_FIELDS:
            time = int(lineParts[TIME_FIELD])
            timeStr = utils.getTimeStrFromInt(time)
            lineParts[TIME_FIELD] = timeStr
        line = ' '.join(lineParts)
        f.write(line)
        if linesIndex < linesLen - 1:
            f.write("\n")
